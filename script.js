"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// gán sự kiện load trang/F5
$(document).ready(onPageLoading);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// function xử lý sựu kiện load trang/F5
function onPageLoading() {
    // Gọi API lấy danh sách khóa học
    let vAjaxRequest = apiGetCoursesList();
    vAjaxRequest.done(function(courses) {
        // Xử lý hiển thị
        console.log(courses);
        loadRecommendedCourses(courses);
        loadMostPopularCourses(courses);
        loadTrendingCourse(courses);
    })
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// function gọi API lấy danh sách khóa học
function apiGetCoursesList() {
    return $.ajax({
        type: "GET",
        url: gBASE_URL + "/courses",
    });
}

// function tạo card thông tin khóa học
function createCourseCard(paramCard, paramCourseData) {
    return paramCard.html(
        `
            <img class="card-img-top" src=${paramCourseData.coverImage}>
            <div class="card-body">
                <p class="text-primary"><b>${paramCourseData.courseName}</b></p>
                <p><i class="fa-regular fa-clock"></i> ${paramCourseData.duration} &nbsp; ${paramCourseData.level}</p>
                <p><span><b>$${paramCourseData.discountPrice}</b></span> <s>$${paramCourseData.price}</s></p>
            </div>
            <div class="card-footer">
                <img src=${paramCourseData.teacherPhoto} class="rounded-circle d-inline-block" style="width: 40px">
                <p class="d-inline-block ml-3"><small>${paramCourseData.teacherName}</small></p>
                <i role="button" class="fa-regular fa-bookmark my-auto float-right"></i>
            </div>
        `
    );
}

// function load recommended course
function loadRecommendedCourses(paramCourse) {
    for (let bIndex = 0; bIndex < 4; bIndex ++) {
        let bNewCard = $("<card>").addClass("card card-hover").data("id", paramCourse[bIndex].id).appendTo("#card-deck-recommended");
        createCourseCard(bNewCard, paramCourse[bIndex]);
    }
}

// function load recommended course
function loadMostPopularCourses(paramCourse) {
    // dùng filter lấy danh sách khóa học most popular
    let vPopularCourses = paramCourse.filter(function(course) {
        return course.isPopular == true;
    });
    // tạo card các khóa học popular (tối đa 4)
    for (let bIndex = 0; bIndex < 4; bIndex ++) {
        let bNewCard = $("<card>").addClass("card card-hover").data("id", vPopularCourses[bIndex].id).appendTo("#card-deck-popular");
        createCourseCard(bNewCard, vPopularCourses[bIndex]);
    }
}

// function load trending course
function loadTrendingCourse(paramCourse) {
    // dùng filter lấy danh sách khóa học trending
    let vTrendingCourse = paramCourse.filter(function(course) {
        return course.isTrending == true;
    });
    // tạo card các khóa học trending (tối đa 4)
    for (let bIndex = 0; bIndex < 4; bIndex ++) {
        let bNewCard = $("<card>").addClass("card card-hover").data("id", vTrendingCourse[bIndex].id).appendTo("#card-deck-trending");
        createCourseCard(bNewCard, vTrendingCourse[bIndex]);
    }
}